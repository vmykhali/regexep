/* eslint-disable radix */
/* eslint-disable prettier/prettier */
task1 = function (inputStr) {
    return inputStr.match(/a.b/g) || [];
}

task2 = function (inputStr) {
    return inputStr.match(/a..a/g) || [];
}

task3 = function (inputStr) {
    return inputStr.match(/a[^c\s]{2}a/g) || [];
}

task4 = function (inputStr) {
    return inputStr.match(/a\da/g) || [];
}

task5 = function (inputStr) {
    return inputStr.match(/ab*a/g) || [];
}

task6 = function (inputStr) {
    return inputStr.match(/ab?a/g) || [];
}

task7 = function (inputStr) {
    return inputStr.match(/ab*a/g) || [];
}

task8 = function (inputStr) {
    return inputStr.match(/(ab)+/g) || [];
}

task9 = function (inputStr) {
    return inputStr.match(/a\.a/g) || [];
}

task10 = function (inputStr) {
    return inputStr.match(/\d\+\d/g) || [];
}

task11 = function (inputStr) {
    return inputStr.match(/\d\++\d/g) || [];
}

task12 = function (inputStr) {
    return inputStr.match(/2\+*3/g) || [];
}

task13 = function (inputStr) {
    return inputStr.match(/\*q+\+/g) || [];
}

task14 = function (inputStr) {
    return inputStr.replace(/\ba[^a]+a\b/g, str => str.replace(/a/g, '!'));
}

task15 = function (inputStr) {
    return inputStr.match(/ab{2,4}a/g) || [];
}

task16 = function (inputStr) {
    return inputStr.match(/ab{1,3}a/g) || [];
}

task17 = function (inputStr) {
    return inputStr.match(/ab{4,}a/g) || [];
}

task18 = function (inputStr) {
    return inputStr.match(/\ba\da\b/g) || [];
}

task19 = function (inputStr) {
    return inputStr.match(/\ba\d+a\b/g) || [];
}

task20 = function (inputStr) {
    return inputStr.match(/\ba\d*a\b/g) || [];
}

task21 = function (inputStr) {
    return inputStr.match(/\ba[^\d]b\b/g) || [];
}

task22 = function (inputStr) {
    return inputStr.match(/\ba[^(\d\w)]b\b/g) || [];
}

task23 = function (inputStr) {
    return inputStr.replace(/\s/g, "!");
}

task24 = function (inputStr) {
    return inputStr.match(/\ba[^(zc)]a\b/g) || [];
}

task25 = function (inputStr) {
    return inputStr.match(/\ba[^(eczx)]a\b/g) || [];
}

task26 = function (inputStr) {
    return inputStr.match(/\ba[3-7]a\b/g) || [];
}

task27 = function (inputStr) {
    return inputStr.match(/\ba[a-g]a\b/g) || [];
}

task28 = function (inputStr) {
    return inputStr.match(/\ba[a-fj-z]a\b/g) || [];
}

task29 = function (inputStr) {
    return inputStr.match(/\ba[a-fA-Z]a\b/g) || [];
}

task30 = function (inputStr) {
    return inputStr.match(/\ba[^(ex)]a\b/g) || [];
}

task31 = function (inputStr) {
    return inputStr.match(/\bw\p{sc=Cyrillic}w\b/gu) || [];
}

task32 = function (inputStr) {
    return inputStr.match(/\ba[a-z]+a\b/g) || [];
}

task33 = function (inputStr) {
    return inputStr.match(/\ba[a-zA-Z]+a\b/g) || [];
}

task34 = function (inputStr) {
    return inputStr.match(/\ba[a-z1-9]+a\b/g) || [];
}

task35 = function (inputStr) {
    return inputStr.match(/[а-яА-ЯЁё]+/g) || [];
}

task36 = function (inputStr) {
    return inputStr.replace(/^aaa/, "!");
}

task37 = function (inputStr) {
    return inputStr.replace(/aaa$/, "!");
}

task38 = function (inputStr) {
    return inputStr.match(/\ba[ex]+a\b/g) || [];
}

task39 = function (inputStr) {
    return inputStr.match(/\ba(e{2}|x+)a\b/g) || [];
}

task40 = function (inputStr) {
    return inputStr.replace(/a\\a/g, "!");
}

task41 = function (inputStr) {
    return inputStr.replace(/a\\\\\\a/g, "!");
}

task42 = function (inputStr) {
    return inputStr.replace(/\/[a-z]+?\\/g, str => str.replace(/[a-z]+/, '!'));
}

task43 = function (inputStr) {
    return inputStr.replace(/\b(\w+)@(\w+)\b/g, '$2@$1');
}

task44 = function (inputStr) {
    return inputStr.replace(/\d/g, match => `${match}${match}`);
}

task45 = function (inputStr) {
    const reg = new RegExp(/[-_.a-z0-9]{1,30}@(mail|yandex)\.[a-z]{2,3}/);
    return reg.test(inputStr) || [];
}

task46 = function (inputStr) {
    return inputStr.match(/[-_.a-z0-9]{1,30}@(mail|yandex)\.[a-z]{2,3}/g) || [];
}

task47 = function (inputStr) {
    const reg = new RegExp(/([(http)(https)]:\/\/)?\w{1,30}\.[a-z]{2,3}/);
    return reg.test(inputStr) || [];
}

task48 = function (inputStr) {
    const reg = new RegExp(/([(http)(https)]:\/\/)?\w{1,30}\.[a-z]{2,3}/);
    return reg.test(inputStr) || [];
}

task49 = function (inputStr) {
    const reg = new RegExp(/([(http)(https)]:\/\/)?\w{1,30}\.[a-z]{2,3}/);
    return reg.test(inputStr) || [];
}

task50 = function (inputStr) {
    const reg = new RegExp(/^(https)/);
    return reg.test(inputStr);
}

task51 = function (inputStr) {
    const reg = new RegExp(/(txt|php|html)$/);
    return reg.test(inputStr) || [];
}

task52 = function (inputStr) {
    const reg = new RegExp(/(hpg|jpeg)$/);
    return reg.test(inputStr) || [];
}

task53 = function (inputStr) {
    const reg = new RegExp(/^(\d){1,12}$/);
    return reg.test(inputStr) || [];
}

task54 = function (inputStr) {
    return inputStr.match(/\d/g) ? inputStr.match(/\d/g).reduce((a, b) => parseInt(a) + parseInt(b), 0) : [];
}

task55 = function (inputStr) {
    return inputStr.replace(/(http:\/\/)(\w{1,30}\.[a-z]{2,3})/, `<a href=\"$1$2\">$2</a>`);
}

task56 = function (inputStr) {
    return inputStr.replace(/\s+/g, " ");
}

task57 = function (inputStr) {
    return inputStr.replace(/\/\*+.*?\*\//g, "");
}

task58 = function (inputStr) {
    return inputStr.replace(/<!--.*?(-->)/g, "");
}

task59 = function (inputStr) {
    return inputStr.replace(/a{3}(?=b)/g, `!`);
}

task60 = function (inputStr) {
    return inputStr.replace(/a{3}(?!b)/g, `!`);
}

task61 = function (inputStr) {
    return inputStr.replace(/\d/g, match => `${match * match}`);
}

task62 = function (inputStr) {
    return inputStr.replace(/(?<=\')\d(?=\')/g, match => `${match * 2}`);
}

task63 = function (inputStr) {
    return inputStr.replace(/(?<=(\{\{))([а-я]+)(?=(\}\}))/g, match => match.split("").reverse().join(""));
}

task64 = function (inputStr) {
    const outPut = inputStr.match(/\d+(?=(\s?\+))|(?<=(\+\s?))\d+/g);
    return `${outPut[0]} + ${outPut[1]} = ${+outPut[0] + (+outPut[1])}`;
}

task65 = function (inputStr) {

    return /^(19\d\d|20\d\d|2100)$/.test(inputStr);
}

task66 = function (inputStr) {
    return /^([01]\d|2[0-3]):[0-5]\d$/.test(inputStr);
}

task67 = function (inputStr) {
    return /^([01]\d|2[0-3]):[0-5]\d\s[ap]m$/.test(inputStr);
}

task68 = function (inputStr) {
    return inputStr.replace(/\W*\w*(\w)\1\w*\W*/g, "");
}

task69 = function (inputStr) {
    return inputStr.replace(/\b(\w+)\s+\1/g, "$1");
}

task70 = function (inputStr) {
    return inputStr.replace(/\b(\w+)\b(?:\s+\1\b)+/g, "$1");
}

module.exports = {};